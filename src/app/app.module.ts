import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NavigationComponent } from './components/molecules/navigation/navigation.component';
import { AccueilComponent } from './accueil/accueil.component';
import { DiplomeComponent } from './diplome/diplome.component';
import { CompetencesComponent } from './competences/competences.component';
import { PortraitComponent } from './portrait/portrait.component';
import {ParticlesModule} from 'angular-particle';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AccueilComponent,
    DiplomeComponent,
    CompetencesComponent,
    PortraitComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ParticlesModule,
    MatButtonModule,
    MatSidenavModule
  ],
  exports: [
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
