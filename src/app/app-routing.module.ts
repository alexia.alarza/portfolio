import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccueilComponent} from './accueil/accueil.component';
import {DiplomeComponent} from './diplome/diplome.component';
import {CompetencesComponent} from './competences/competences.component';
import {PortraitComponent} from './portrait/portrait.component';

const routes: Routes = [
  {
    path: '',
    component: AccueilComponent
  },
  {
    path: 'diplome',
    component: DiplomeComponent
  },
  {
    path: 'competences',
    component: CompetencesComponent
  },
  {
    path: 'portrait',
    component: PortraitComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
